#!/usr/bin/env python
# -*- coding: latin-1 -*-
from lxml import html
import requests
import pprint


def get_votes_on_page(link):
    page_ = requests.get(link)
    tree_ = html.fromstring(page_.text)
    post_authors = tree_.xpath('//div[@class="postauthor"]/text()')
    post_ranks = tree_.xpath('//div[@class="posterrank"]/text()')
    post_contents = tree_.xpath('//div[@class="postbody"]/text()')
    
    posts = []
    for i in range(len(post_authors)):
        post_data_ = {}
        post_data_["author"] = post_authors[i]
        post_data_["content"] = post_contents[i]
        post_data_["rank"] = post_ranks[i]
        posts.append(post_data_)
    #pprint.pprint(posts)
    post_authors = None
    post_ranks = None
    post_contents = None
    return posts


def get_pages(filename):
    page_file = open(filename, 'r')
    links = []
    for line in page_file:
        links.append(line.strip())
    return links


def cycle_pages(page_links):
    post_datas = []
    for page in page_links:
        post_data = get_votes_on_page(page)
        post_datas = post_datas + post_data
    return post_datas


if __name__ == '__main__':
    pages = get_pages("list_pages.txt")
    all_data = cycle_pages(pages)
    pprint.pprint(all_data)
    print "Made by Maez"
