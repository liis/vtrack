What is VTrack?
===============

This is a utility designed to simplify the counting of votes, in any USG forum. It searches for the postauthor, posterrank and postcontent div classes, and uses these to quickly compile the vote counts into a more readable format.


How to Use the Utility
======================
You need to be running Python 2.7 (this was developed using Python 2.7.5 on Windows 8.1, 64-bit) in order for this to work. In addition, this utility makes use of the **lxml** and **requests** libraries, both of which need to be installed prior to use.

To use, simply run the ``python vtrack.py`` program in a directory where the file ``list_pages.txt`` is located. Simply copy and paste the all of the pages of each bill into the ``list_pages.txt`` file, one per line. 

::
    
    #An Example list_pages.txt file

    http://www.usgovsim.net/USG/viewtopic.php?f=292&t=17633
    http://www.usgovsim.net/USG/viewtopic.php?f=292&t=17633&start=20

::

    # Example Commands (need to be run in the Command Line)
    python vtrack.py
    python vtrack.py > output.txt

Note that with the second you can open the ``output.txt`` file for easier copying and pasting

::
    
    # Example Output
    [{'author': 'Bruce Tedder',
      'content': '72 hours to vote.',
      'rank': 'Democrat'},
     {'author': 'Bruce Tedder', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'Grady', 'content': 'Nay', 'rank': 'Republican'},
     {'author': 'Felledor', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'John Fairbanks', 'content': 'Aye.', 'rank': 'Democrat'},
     {'author': 'Earnshaw', 'content': 'Aye.', 'rank': 'Democrat'},
     {'author': 'Melissa Knox', 'content': 'Nay', 'rank': 'Independent'},
     {'author': 'Maez', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'Pav', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'Nadine', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'Booker', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'Richards', 'content': 'Aye', 'rank': 'Republican'},
     {'author': 'Troy Bolton', 'content': 'Nay', 'rank': 'Republican'},
     {'author': 'Marvin', 'content': 'Aye', 'rank': 'Republican'},
     {'author': 'Randall', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'Wellbourne', 'content': 'Aye', 'rank': 'Republican'},
     {'author': 'Julian Stephens', 'content': 'Nay', 'rank': 'Republican'},
     {'author': 'The Ricktator', 'content': 'aye', 'rank': 'Republican'},
     {'author': 'Bingham', 'content': 'Aye!', 'rank': 'Democrat'},
     {'author': 'Smith', 'content': 'Aye', 'rank': 'Republican'},
     {'author': 'Charles Kang', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'Price', 'content': 'aye', 'rank': 'Republican'},
     {'author': 'Tyler', 'content': 'Aye', 'rank': 'Republican'},
     {'author': 'Brian Caruso', 'content': 'Nay', 'rank': 'Republican'},
     {'author': 'McCoy', 'content': 'aye', 'rank': 'Republican'},
     {'author': 'Fuller', 'content': 'Aye', 'rank': 'Republican'},
     {'author': 'Tom Jacks', 'content': 'nay', 'rank': 'Republican'},
     {'author': 'Malcom Gray', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'Katherine Whitmore', 'content': 'Nay', 'rank': 'Republican'},
     {'author': 'Alvarez', 'content': 'Aye', 'rank': 'Democrat'},
     {'author': 'Bruce Tedder',
      'content': 'Whitmore and Alvarez late.',
      'rank': 'Democrat'}]
    Made by Maez
